(use-modules
 (gnu)
 (gnu home)
 (gnu home services)
 (gnu home services shells)
 (gnu home services shepherd)
 (gnu packages)
 (gnu packages linux)
 (gnu packages admin)
 (gnu packages shells)
 (gnu packages pulseaudio)
 (gnu packages web-browsers)
 (gnu packages guile)
 (gnu packages wget)
 (gnu packages curl)
 (gnu packages pdf)
 (gnu packages ebook)
 (gnu packages compton)
 (gnu packages dunst)
 (gnu packages image-viewers)
 (gnu packages gnome)
 (gnu packages gnome-xyz)
 (gnu packages compression)
 (gnu packages cmake)
 (gnu packages autotools)
 (gnu packages gstreamer)
 (gnu packages video)
 (gnu packages mpd)
 (gnu packages terminals)
 (gnu packages xdisorg)
 (gnu packages gnupg)
 (gnu packages password-utils)
 (gnu packages emacs)
 (gnu packages emacs-xyz)
 (gnu packages gnuzilla)
 (gnu packages ruby)
 (gnu packages python)
 (gnu packages lisp)
 (gnu packages python-xyz)     
 (gnu packages fonts)
 (gnu packages wm)
 (gnu packages music)
 (gnu packages base)
 (gnu packages xorg)
 (gnu packages dns)
 (gnu packages kde-frameworks)
 (gnu services)
 (guix gexp))

(home-environment
 (packages
  (append
   (list
    ;; Interfacing
    xclip
    xset
    xss-lock
    polybar
    playerctl
    xbacklight
    xmodmap
    rofi
    dunst
    arandr
    feh
    picom)
   ;; Admin
   (list
    inetutils
    wget
    curl
    zip
    seahorse
    network-manager
    networkmanager-qt
    network-manager-applet)
   ;; Tools/Libraries
   (list
    unzip
    zip
    cmake
    libtool
    gst-plugins-bad
    gst-plugins-base
    gst-plugins-good
    gst-plugins-ugly
    gst-libav
    playerctl
    gnu-make)
   ;; Applications
   (list
    alacritty
    nyxt
    zathura-pdf-mupdf
    mpv
    mpd
    ncmpcpp
    icedove
    keepassxc
    nautilus
    pavucontrol)
   ;; Emacs
   (list
    emacs-vterm
    emacs-geiser
    emacs-geiser-guile
    emacs-doom-modeline
    emacs-all-the-icons
    emacs-all-the-icons-dired
    emacs-general
    emacs-which-key
    emacs-alert
    emacs-helpful
    emacs-pinentry
    emacs-doom-themes
    emacs-evil
    emacs-evil-collection
    emacs-vertico
    emacs-orderless
    emacs-consult
    emacs-company
    emacs-marginalia
    emacs-perspective
    emacs-dired-rsync
    emacs-dired-hacks
    emacs-org-bullets
    emacs-org-make-toc
    emacs-visual-fill-column
    emacs-flycheck
    emacs-rainbow-mode
    emacs-rainbow-delimiters
    emacs-magit
    emacs-projectile
    emacs-lsp-mode
    emacs-lsp-ui
    emacs-treemacs
    emacs-lsp-treemacs
    emacs-parinfer-mode
    emacs-yasnippet
    emacs-pdf-tools)
   ;; Languages
   (list
    ruby
    picolisp
    python
    guile-3.0
    sbcl
    python-lsp-server
    ruby-solargraph)
   ;; Aesthetics/Fonts
   (list
    gnome-tweaks
    papirus-icon-theme
    arc-theme
    font-fira-mono
    font-fira-code
    font-fira-sans
    font-abattis-cantarell
    font-google-noto
    font-awesome
    pfetch)))
 (services
  (append
   ;; Shell
   (list
    (service home-zsh-service-type
             (home-zsh-configuration
              ;; Place all configs to $XDG_CONFIG_HOME/zsh. Makes
              ;; ~/.zshenv set ZDOTDIR to $XDG_CONFIG_HOME/zsh
              ;; and shell process starts with ~/.config/zsh/.zshenv
              (xdg-flavor? #t)
              ;; .zshenv
              (zshenv
               (list
                (local-file "files/zsh/zshenv")))
              ;; .zprofile
              (zprofile
               (list
                (local-file "files/zsh/zprofile")))
              ;; .zshrc
              (zshrc
               (list
                (local-file "files/zsh/zshrc"))))))

   ;; Files
   (list
    (simple-service 'i3-config
                    home-files-service-type
                    (list
                     `("config/i3/config"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/i3/config")))))
    (simple-service 'i3status-config
                    home-files-service-type
                    (list
                     `("config/i3status/config"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/i3status/config")))))
    (simple-service 'picom-config
                    home-files-service-type
                    (list
                     `("config/picom/picom.conf"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/picom/picom.conf")))))
    (simple-service 'dunst-config
                    home-files-service-type
                    (list
                     `("config/dunst/dunstrc"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/dunst/dunstrc")))))
    (simple-service 'rofi-config
                    home-files-service-type
                    (list
                     `("config/rofi/config.rasi"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/rofi/config.rasi")))))
    (simple-service 'zsh-aliases
                    home-files-service-type
                    (list
                     `("config/zsh/zsh_aliases"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/zsh/zsh_aliases")))))
    (simple-service 'zsh-functions
                    home-files-service-type
                    (list
                     `("config/zsh/zsh_functions"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/zsh/zsh_functions")))))
    (simple-service 'redshift-config
                    home-files-service-type
                    (list
                     `("config/redshift/redshift.conf"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/redshift/redshift.conf")))))
    (simple-service 'xmodmap-config
                    home-files-service-type
                    (list
                     `("config/xmodmap/xmodmap"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/xmodmap/xmodmap")))))
    (simple-service 'user-dirs-config
                    home-files-service-type
                    (list
                     `("config/user-dirs.dirs"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/user-dirs.dirs")))))
    (simple-service 'emacs-config
                    home-files-service-type
                    (list
                     `("config/emacs/init.el"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/emacs/init.el")))))
    (simple-service 'nyxt-config
                    home-files-service-type
                    (list
                     `("config/nyxt/init.lisp"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/nyxt/init.lisp")))))
    (simple-service 'mbsync-config
                    home-files-service-type
                    (list
                     `("config/mbsync/mbsyncrc"
                       ,(local-file
                         (string-append (getenv "HOME")
                                        "/dotfiles/guix/home/files/mbsync/mbsyncrc")))))

    (simple-service 'alacritty-config
			              home-files-service-type
			              (list
			               `("config/alacritty/alacritty.yml"
			                 ,(local-file
			                   (string-append (getenv "HOME")
					                              "/dotfiles/guix/home/files/alacritty/alacritty.yml"))))))
    ;; Daemons
    (list
     (service home-shepherd-service-type
              (home-shepherd-configuration
		           (services
		            (list
		             (shepherd-service
			            (provision '(emacs))
			            (start #~(make-system-constructor "emacs --daemon"))
                  (stop #~(make-system-constructor "emacsclient -e '(kill-emacs)'")))
                 (shepherd-service
			            (provision '(gnome-keyring-daemon))
			            (start #~(make-system-contructor "gnome-keyring-daemon --daemonize "))
			            (stop #~(make-system-contructor "pkill gnome-keyring-daemon")))))))))))
