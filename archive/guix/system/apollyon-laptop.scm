(use-modules (gnu)
             (gnu packages wm)
             (gnu packages version-control)
             (gnu packages emacs)
             (gnu packages gnuzilla)
             (gnu packages vim)
             (gnu packages certs)
             (gnu packages pulseaudio)
             (gnu packages audio)
             (gnu packages linux)
             (gnu packages suckless)
             (gnu packages terminals)
             (gnu packages gnome)
	     (gnu packages shells)
	     (gnu packages emacs)
             (gnu system setuid)
             (nongnu packages linux))

(use-service-modules
 cups
 desktop
 networking
 ssh
 xorg)

(operating-system
 (kernel linux)
 (firmware (list linux-firmware sof-firmware))
 (locale "en_US.utf8")
 (timezone "America/Los_Angeles")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "apollyon")
 (users (cons* (user-account
                (name "opal")
                (comment "opal")
                (group "users")
                (shell (file-append zsh "/bin/zsh"))
                (home-directory "/home/opal")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))
 (packages
  (append
   (list
     zsh
     emacs
     i3-wm
     i3status
     git
     network-manager
     icecat
     emacs
     bluez
     bluez-alsa
     pulseaudio
     dmenu
     alacritty
     nss-certs)
   %base-packages))
 (services
  (append
   (list (service gnome-desktop-service-type)
         (service openssh-service-type)
         (service tor-service-type)
         (service cups-service-type)
         (set-xorg-configuration
          (xorg-configuration
           (keyboard-layout keyboard-layout))))
   %desktop-services))
 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))
 (mapped-devices
  (list (mapped-device
         (source
          (uuid "ceb0c15f-30b6-4d02-a7e9-96ddcd73c763"))
         (target "cryptroot")
         (type luks-device-mapping))))
 (file-systems
  (cons* (file-system
          (mount-point "/")
          (device "/dev/mapper/cryptroot")
          (type "btrfs")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/boot/efi")
          (device (uuid "739C-796C" 'fat32))
          (type "vfat"))
         %base-file-systems)))
