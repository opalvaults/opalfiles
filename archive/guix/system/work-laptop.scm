(use-modules (gnu)
             (gnu services desktop)
             (gnu packages wm)
             (gnu system setuid)
             (gnu packages admin)
             (nongnu packages linux))

(use-service-modules
 cups
 desktop
 networking
 ssh
 sddm
 xorg)

(operating-system
 (kernel linux)
 (firmware (list linux-firmware))
 (locale "en_US.utf8")
 (timezone "America/Los_Angeles")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "borges")
 (setuid-programs
  (cons (setuid-program
         (program (file-append swaylock "/bin/swaylock")))
        %setuid-programs))
 (users (cons* (user-account
                (name "opal")
                (comment "opal")
                (group "users")
                (home-directory "/home/opal")
                (supplementary-groups
                 '("wheel"
                   "netdev"
                   "audio"
                   "video")))
               %base-user-accounts))

 (packages (append (list git
                         wl-clipboard
                         vim
                         nss-certs
                         stow
                         exfat-utils
                         emacs
                         emacs-exwm
                         sway
                         swaybg
                         swaylock
                         swayidle
                         polybar
                         nyxt
                         dmenu
                         bluez
                         bluez-alsa
                         pulseaudio
                         keepassxc
                         font-fira-mono
                         font-fira-sans
                         font-fira-code
                         gcc-toolchain))
           %base-packages)

 (services
  (append (modify-services %desktop-services
                   (gdm-service-type config
                     = (gdm-configuration
                         (inherit config)
                         (wayland? #t)
                         (debug? #t))))))
  ;; (append (modify-services %desktop-services
  ;;                          (delete gdm-service-type))
  ;;         (list (service sddm-service-type
  ;;                        (sddm-configuration
  ;;                         (display-server "wayland"))))))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))
 (mapped-devices
  (list (mapped-device
         (source
          (uuid "abe760e2-4ba5-4f43-81a2-1c3f16eb62a8"))
         (target "cryptroot")
         (type luks-device-mapping))))
 (file-systems
  (cons* (file-system
          (mount-point "/")
          (device "/dev/mapper/cryptroot")
          (type "btrfs")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/boot/efi")
          (device (uuid "15B3-5DE5" 'fat32))
          (type "vfat"))
         %base-file-systems))


;; (services (cons* ...
;;                  (modify-services %desktop-services
;;                    (gdm-service-type config
;;                      =;;putabrackethere (gdm-configuration
;;                          (inherit config)
;;                          (wayland? #t)
;;                          (debug? #t))))))
