(use-modules (gnu)
             (gnu services desktop)
             (gnu packages wm)
             (gnu packages shells)
             (gnu packages version-control)
             (gnu packages emacs)
             (gnu packages gnuzilla)
             (gnu packages vim)
             (gnu packages certs)
             (gnu packages file-systems)
             (gnu packages suckless)
             (gnu packages admin)
             (gnu packages linux)
             (gnu packages audio)
             (gnu packages pulseaudio)
             (gnu system setuid)
             (nongnu packages linux))

(use-service-modules
 cups
 desktop
 networking
 ssh
 sddm
 xorg)

(operating-system
 (kernel linux)
 (firmware (list linux-firmware))
 (locale "en_US.utf8")
 (timezone "America/Los_Angeles")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "borges")
 (setuid-programs
  (cons (setuid-program
         (program (file-append swaylock "/bin/swaylock")))
        %setuid-programs))
 (users (cons* (user-account
                (name "opal")
                (comment "opal")
                (group "users")
                (home-directory "/home/opal") 
                (shell (file-append zsh "/bin/zsh"))
                (supplementary-groups
                 '("wheel"
                   "netdev"
                   "audio"
                   "video")))
               %base-user-accounts))

 (packages (append (list git
                         icecat
                         vim
                         nss-certs
                         exfat-utils
                         emacs
                         sway
                         swaybg
                         swaylock
                         swayidle
                         dmenu
                         bluez
                         bluez-alsa
                         pulseaudio)
                   %base-packages))


;; (services (cons* ...
;;                  (modify-services %desktop-services
;;                    (gdm-service-type config
;;                      =;;putabrackethere (gdm-configuration
;;                          (inherit config)
;;                          (wayland? #t)
;;                          (debug? #t))))))
 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))
 (mapped-devices
  (list (mapped-device
         (source
          (uuid "40aa6387-e935-4f70-8e7d-1975678a5a32"))
         (target "cryptroot")
         (type luks-device-mapping))))
 (file-systems
  (cons* (file-system
          (mount-point "/")
          (device "/dev/mapper/cryptroot")
          (type "btrfs")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/boot/efi")
          (device (uuid "1C3B-10F5" 'fat32))
          (type "vfat"))
         %base-file-systems)))
