(add-to-list 'load-path "~/.guix-profile/bin/guile")
(add-to-list 'load-path "~/.guix-profile/share/emacs/site-lisp")

;; Match Emacs program path to be the same as user shell
(defun set-exec-path-from-shell-PATH ()
  "Set up Emacs' `exec-path' and PATH environment variable to match
that used by the user's shell."
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string
    "[ \t\n]*$" "" (shell-command-to-string
    "$SHELL --login -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

(set-exec-path-from-shell-PATH)

;; Setting general info
(setq user-full-name "Ry"
      user-mail-address "ry@opal.sh")

;; Set line number
(setq display-line-numbers-type t)

;; Keeps text lines from going off screen.
(set-default 'truncate-lines nil)
;; Add this hook to ERC if I run into trouble with truncated lines in other modes.
;; (add-hook 'erc-mode-hook (lambda () (setq-default truncate-lines nil)))


;; Only enable line numbers for certain modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Enable clipboard
(setq x-select-enable-clipboard t)

;; load theme
(setq doom-theme 'modus-operandi)

;; Configure Modus theme
(use-package modus-themes
  :init
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs nil
        modus-themes-region '(accented bg-only no-extend)
        modus-themes-org-blocks 'greyscale
        modus-themes-paren-match 'intense
        modus-themes-mixed-fonts t)

  ;; Load the theme files before enabling a theme
  (modus-themes-load-themes)
  :config

  (modus-themes-load-vivendi) ;; OR (modus-themes-load-operandi)
  :bind ("<f5>" . modus-themes-toggle))

;; Set fonts
(set-face-attribute 'default nil :font "Fira Code" :height 125 :weight 'medium)
(set-face-attribute 'variable-pitch nil :font "Fira Sans" :height 1.0 :weight 'regular)
(set-face-attribute 'fixed-pitch nil :font "Fira Code" :height 1.0 :weight 'medium)

(defun rymacs/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1)
                                                          "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
  (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

;; Set org agenda dir
(setq org-directory "~/org/")

(defun rymacs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1)
  (setq org-startup-folded t))

(use-package org
  :commands (org-capture org-agenda)
  :hook (org-mode . rymacs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  (setq org-agenda-files
        '("~/org/projects/"
          "~/org/tasks/"
          ))

  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60)

  (setq org-todo-keywords
    '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
      (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))

  (setq org-refile-targets
    '(("archive.org" :maxlevel . 1)
      ("planner.org" :maxlevel . 1)))

  ;; Save Org buffers after refiling!
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  (setq org-tag-alist
    '((:startgroup)
       ; Put mutually exclusive tags here
       (:endgroup)
       ("@errand" . ?E)
       ("@home" . ?H)
       ("@work" . ?W)
       ("agenda" . ?a)
       ("planning" . ?p)
       ("publish" . ?P)
       ("batch" . ?b)
       ("note" . ?n)
       ("idea" . ?i)))

  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
   '(("d" "Dashboard"
     ((agenda "" ((org-deadline-warning-days 7)))
      (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
      (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))

    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))))

  ;; Create capture templates
  (setq org-capture-templates
    `(("t" "Tasks")
      ("tt" "Task" entry (file+olp "~/org/planner/tasks.org" "Inbox")
           "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

      ("p" "Projects")
      ("pp" "Project File" entry (file+olp "~/org/projects/auto-infra-overview.org" "Inbox")
            "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)))
  ;; Init font setup
  (rymacs/org-font-setup))

;; Change default pretty bullets to circles
(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; Creates margins, and centerrs content in org mode.
(defun rymacs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . rymacs/org-mode-visual-fill))

(use-package ob-racket
  :after org
  :config
  (add-hook 'ob-racket-pre-runtime-library-load-hook
          #'ob-racket-raco-make-runtime-library))

;; Load languages for babel code blocks.
(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (racket . t)
     (python . t)
     (scheme . t)
     (javascript . t)
     (html . t)
     (css . t)
     (lisp . t)))

  (push '("conf-unix" . conf-unix) org-src-lang-modes))

(setq geiser-default-implementation '(guile))

;; Make shortcuts to easily create babel source code blocks.
(with-eval-after-load 'org
  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python"))
  (add-to-list 'org-structure-template-alist '("yml" . "src yaml"))
  (add-to-list 'org-structure-template-alist '("scm" . "src scheme"))
  (add-to-list 'org-structure-template-alist '("js" . "src javascript"))
  (add-to-list 'org-structure-template-alist '("html" . "src html"))
  (add-to-list 'org-structure-template-alist '("css" . "src css"))
  (add-to-list 'org-structure-template-alist '("rt" . "src racket"))
  (add-to-list 'org-structure-template-alist '("cl" . "src lisp")))

;; ;; Define a function that automatically executes rymacs/org-babel-tangle-config (a wrapper around org-babel-tangle) when saving this file.
;; (defun rymacs/org-babel-tangle-config ()
;;   (when (string-equal (file-name-directory (buffer-file-name))
;;                       (expand-file-name "~/.dotfiles/.config/doom"))

;;      (let ((org-confirm-babel-evaluate nil))
;;        (org-babel-tangle))))

;; (add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'rymacs/org-babel-tangle-config)))

(after! sly
  (load "/home/ry/quicklisp/clhs-use-local.el" t)
  (setq sly-lisp-implementations
        '((sbcl ("/bin/sbcl" "-L" "sbcl" "-Q" "run") :coding-system utf-8-unix)
          (ccl ("/usr/local/bin/ccl64" :coding-system utf-8-unix)))))

(defmacro define-sly-lisp (name)
  `(defun ,name ()  (interactive)  (let ((sly-default-lisp ',name))  (sly))))

(define-sly-lisp sbcl)
(define-sly-lisp ccl)

(use-package mu4e
  :config
  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Refresh mail using isync every 5 minutes
  (setq mu4e-update-interval (* 5 60))
  (setq mu4e-get-mail-command "mbsync -a -c ~/Dotfiles/.config/mbsync/mbsyncrc")
  (setq mu4e-maildir "~/Mail")

  (setq mu4e-contexts
        (list
         ;; Opal.sh
         (make-mu4e-context
          :name "Ry P."
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/opal.sh" (mu4e-message-field msg :maildir))))

          :vars '((user-mail-address . "ry@opal.sh")
                  (user-full-name . "Ry P.")
                  (mu4e-drafts-folder . "/opal.sh/Drafts")
                  (mu4e-sent-folder . "/opal.sh/Sent")
                  (mu4e-trash-folder . "/opal.sh/Trash")))))

  (setq mu4e-maildir-shortcuts
        '(("/opal.sh/Inbox" . ?i)
          ("/opal.sh/Sent"  . ?s)
          ("/opal.sh/Trash" . ?t)
          ("/opal.sh/Drafts" . ?d))))

(setq erc-server "irc.libera.chat" ;sets default server
      erc-nick "opalvaults" ; Sets nick
      erc-user-full-name "opalvaults"
      erc-track-shorten-start 8
      erc-kill-buffer-on-part t
      erc-auto-query 'bury
      erc-fill-column 90
      erc-fill-function 'erc-fill-static
      erc-fill-static-center 20
      erc-track-visibility nil
      erc-interpret-mirc-color t
      erc-rename-buffers t
      erc-track-exclude-server-buffer t)
