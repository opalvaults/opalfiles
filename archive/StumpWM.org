#+TITLE: StumpWM Configuration
#+PROPERTY: :tangle ~/Dotfiles/.config/stumpwm/config :mkdirp yes

In my quest to make my computer a Lisp machine in its own right, I am using StumpWM as my window manager.

* Init
#+begin_src lisp
;;; -*-  mode: lisp; -*-
(in-package :stumpwm)
;; (defvar *sbcl-path* "~/.guix-home/profile/share/common-lisp/sbcl/")

;; StumpWM Modules
(stumpwm:add-to-load-path "~/.guix-home/profile/share/common-lisp/sbcl/stumpwm-swm-gaps")
(stumpwm:add-to-load-path "~/.guix-home/profile/share/common-lisp/sbcl/stumpwm-ttf-fonts")
(stumpwm:add-to-load-path "~/.guix-home/profile/share/common-lisp/sbcl/stumpwm-stumptray")
(stumpwm:add-to-load-path "~/.guix-home/profile/share/common-lisp/sbcl/stumpwm-kbd-layouts")
#+end_src

* Environment
** Workspaces
#+begin_src lisp
(run-commands
 "gnewbg I"
 "gnew II"
 "gnewbg III"
 "gnewbg IV"
 "gnewbg V"
 "gnewbg-float F")
#+end_src

** Set Prefix
#+begin_src lisp
(set-prefix-key (kbd "s-ESC"))
#+end_src

** Focus window with mouse
#+begin_src lisp
(setf *mouse-focus-policy* :click)
#+end_src

** Messages
#+begin_src lisp
(setf *message-window-gravity* :center
      *input-window-gravity* :center
      *window-border-style* :thin
      *message-window-padding* 3
      *maxsize-border-width* 2
      *normal-border-width* 2
      *transient-border-width* 2
      stumpwm::*float-window-border* 1
      stumpwm::*float-window-title-height* 1)
#+end_src

** Command Window
#+begin_src lisp
(setq *input-window-gravity* :center)
#+end_src
* Keyboard
** Load Keyboard Layout Module
#+begin_src lisp
(load-module "kbd-layouts")
#+end_src
** Remap Caps Lock
#+begin_src lisp :tangle yes
(setf kbd-layouts:*caps-lock-behavior* :esc)
#+end_src
** Xmodmap
#+begin_src lisp
(run-shell-command "xmodmap ~/Dotfiles/.config/Xmodmap")
#+end_src

** Keybinds
#+begin_src lisp
;; Set some super key bindings
(define-key *top-map* (kbd "s-h") "move-focus left")
(define-key *top-map* (kbd "s-l") "move-focus right")
(define-key *top-map* (kbd "s-j") "move-focus down")
(define-key *top-map* (kbd "s-k") "move-focus up")

(define-key *top-map* (kbd "s-H") "move-window left")
(define-key *top-map* (kbd "s-L") "move-window right")
(define-key *top-map* (kbd "s-J") "move-window down")
(define-key *top-map* (kbd "s-K") "move-window up")

(define-key *top-map* (kbd "s-f") "fullscreen")
(define-key *top-map* (kbd "s-r") "iresize")
(define-key *top-map* (kbd "s-Q") "delete")
(define-key *top-map* (kbd "s-SPC") "run-shell-command emacsclient -e \"(call-interactively #'app-launcher-run-app)\"")
(define-key *top-map* (kbd "s-L") "run-shell-command slock")
(define-key *top-map* (kbd "s-E") "run-shell-command emacs")
(define-key *top-map* (kbd "s-W") "run-shell-command firefox")

(define-key *top-map* (kbd "s-TAB") "next-in-frame")
(define-key *top-map* (kbd "s-S-TAB") "prev-in-frame")

(define-key *top-map* (kbd "s-1") "gselect I")
(define-key *top-map* (kbd "s-2") "gselect II")
(define-key *top-map* (kbd "s-3") "gselect III")
(define-key *top-map* (kbd "s-4") "gselect IV")
(define-key *top-map* (kbd "s-5") "gselect V")

(define-key *top-map* (kbd "s-!") "gmove I")
(define-key *top-map* (kbd "s-@") "gmove II")
(define-key *top-map* (kbd "s-#") "gmove III")
(define-key *top-map* (kbd "s-$") "gmove IV")
(define-key *top-map* (kbd "s-%") "gmove V")

;; Brightness and volume
(define-key *top-map* (kbd "XF86MonBrightnessDown") "run-shell-command brightnessctl set 5%-")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "run-shell-command brightnessctl set +5%")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "run-shell-command amixer set Master 3%- unmute")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "run-shell-command amixer set Master 3%+ unmute")
(define-key *top-map* (kbd "XF86AudioMute") "run-shell-command amixer set Master toggle")
#+end_src

* UI
** Colors
#+begin_src lisp
;; Set UI colors
(setf *colors*
      '("#000000"   ;black
        "#BF6262"   ;red
        "#a1bf78"   ;green
        "#dbb774"   ;yellow
        "#7D8FA3"   ;blue
        "#ff99ff"   ;magenta
        "#53cdbd"   ;cyan
        "#b7bec9")) ;white

(setf *default-bg-color* "#e699cc")

(update-color-map (current-screen))

(setf *window-format* "%m%s%50t")
#+end_src
** Gaps
#+begin_src lisp
(load-module "swm-gaps")
(setf swm-gaps:*inner-gaps-size* 3)
(run-commands "toggle-gaps-on")
#+end_src
** Fonts
#+begin_src lisp
Enable TTF fonts
(load-module "ttf-fonts")
(setf xft:*font-dirs* '("/home/opal/.guix-home/profile/share/fonts/"))
(xft:cache-fonts)
(set-font (make-instance 'xft:font :family "Fira Mono" :subfamily "Regular" :size 16))
#+end_src
** Mode-Line
#+begin_src lisp
(setf *mode-line-background-color* (car *colors*)
      *mode-line-foreground-color* (car (last *colors*))
      *mode-line-timeout* 1)
#+end_src

** System tray
#+begin_src lisp
;; Add the system tray module
(load-module "stumptray")

(stumptray:stumptray)
#+end_src

* Applications
#+begin_src lisp 
(run-shell-command "emacs")
#+end_src

* Repl Start
#+begin_src lisp 
(require :slynk)
(slynk:create-server :dont-close t)
#+end_src

* Reference
- https://github.com/daviwil/dotfiles/blob/guix-home/Stump.org
- https://github.com/alezost/stumpwm-config
- https://github.com/herbertjones/my-stumpwm-config
- https://github.com/Phundrak/dotfiles/blob/master/org/config/stumpwm.org
 
