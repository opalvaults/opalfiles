#!/bin/bash

#Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

#Enable bash programmable completion features in interactive shells
if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi


#######################################################
# EXPORTS
#######################################################

# Expand the history size
export HISTFILESIZE=99999
export HISTSIZE=99999

# Don't put duplicate lines in the history and do not add lines that start with a space
export HISTCONTROL=erasedups:ignoredups:ignorespace

# Check the window size after each command and, if necessary, update the values of LINES and COLUMNS
shopt -s checkwinsize

# Causes bash to append to history instead of overwriting it so if you start a new terminal, you have old session history
shopt -s histappend
PROMPT_COMMAND='history -a'

# Allow ctrl-S for history navigation (with ctrl-R)
stty -ixon

# Set the default editor
export EDITOR=vim
export VISUAL=vim

# To have colors for ls and all grep commands such as grep, egrep and zgrep
export CLICOLOR=1
export LS_COLORS='no=00:fi=00:di=00;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:*.xml=00;31:'
alias grep="/usr/bin/grep $GREP_OPTIONS"
unset GREP_OPTIONS

# Color for manpages in less makes manpages a little easier to read
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

export LC_WHO=rorlando

# Scripts Path
PATH=$PATH:~/.local/bin
PATH=$PATH:~/bin
PATH=$PATH:/usr/local/go/bin

# Doom Path
PATH=$PATH:~/.config/emacs/bin/

# Flatpak path
PATH=$PATH:/var/lib/flatpak/exports/bin
PATH=$PATH:/var/lib/flatpak/app


#######################################################
# GOPASS
#######################################################

# Ensure gopass autocompletion is sourced
source <(gopass completion bash)

# Gopass aliases
alias gpi='gopass insert --multiline'
alias gpl='gopass ls'
alias gps='gopass show'
alias gpsclip='gopass show --clip'
alias gpe='gopass edit'


#######################################################
# ALIAS
#######################################################
# To temporarily bypass an alias, we preceed the command with a \
# EG: the ls command is aliased, but to use the normal ls command you would type \ls

alias v='nvim'
alias vim='nvim'
alias vi='nvim'
#comment

# Apt aliases
alias install='sudo apt install'
alias update='sudo apt update'
alias update='sudo apt upgrade'
alias uu='sudo apt update && sudo apt upgrade'
alias search='apt search'
alias remove='sudo apt remove'

# alias to show the date
alias da='date "+%Y-%m-%d %A %T %Z"'

# Alias's to modified commands
alias cp='cp -i'
alias mv='mv -i'
alias rm='echo "Please use trash instead"'
alias mkdir='mkdir -p'
alias ps='ps auxf'
alias ping='ping -c 10'
alias less='less -R'
alias vi='vim'
alias vis='vim "+set si"'

# Change directory aliases
alias home='cd ~'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# cd into the old directory
alias bd='cd "$OLDPWD"'

# ls -> exa
alias ls='exa --icons'

# Search command line history
alias h="history | grep "

# Search files in the current folder
alias f="find . | grep "

# Show current network connections to the server
alias ipview="netstat -anpl | grep :80 | awk {'print \$5'} | cut -d\":\" -f1 | sort | uniq -c | sort -n | sed -e 's/^ *//' -e 's/ *\$//'"

# Alias's to show disk space and space used in a folder
alias diskspace="du -S | sort -n -r |more"
alias folders='du -h --max-depth=1'
alias folderssort='find . -maxdepth 1 -type d -print0 | xargs -0 du -sk | sort -rn'
alias tree='tree -CAhF --dirsfirst'
alias treed='tree -CAFd'
alias mountedinfo='df -hT'

# Alias's for archives
alias mktar='tar -cvf'
alias mkbz2='tar -cvjf'
alias mkgz='tar -cvzf'
alias untar='tar -xvf'
alias unbz2='tar -xvjf'
alias ungz='tar -xvzf'

# Wget (keeps hsts files out of $HOME)
alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'

# Translate
alias tbr='trans :pt-BR'


#######################################################
# WORK
#######################################################

# Wireguard
alias wgup='sudo wg-quick up /etc/wireguard/wg0.conf'
alias wgdown='sudo wg-quick down /etc/wireguard/wg0.conf'

# SSH
alias stork='ssh stork'
alias mail='ssh mail'
alias eel='ssh eel'

# Keychain
alias keys='eval $(keychain --eval --quiet ry_ecdsa) && eval $(keychain --eval --quiet id_rsa)'


#######################################################
# SPECIAL FUNCTIONS
#######################################################

# Create and go to the directory
mkdirg() {
    mkdir -p $1
    cd $1
}


#######################################################
# Prompt
#######################################################

function __setprompt {

    # Define colors
    local LIGHTGRAY="\033[0;37m"
    local WHITE="\033[1;37m"
    local BLACK="\033[0;30m"
    local DARKGRAY="\033[1;30m"
    local RED="\033[0;31m"
    local LIGHTRED="\033[1;31m"
    local GREEN="\033[0;32m"
    local LIGHTGREEN="\033[1;32m"
    local BROWN="\033[0;33m"
    local YELLOW="\033[1;33m"
    local BLUE="\033[0;34m"
    local LIGHTBLUE="\033[1;34m"
    local MAGENTA="\033[0;35m"
    local LIGHTMAGENTA="\033[1;35m"
    local CYAN="\033[0;36m"
    local LIGHTCYAN="\033[1;36m"
    local NOCOLOR="\033[0m"
    local PURPLE="\033[1;34m"

    PS1=""
    # User and server
    local SSH_IP=$(echo $SSH_CLIENT | awk '{ print $1 }')
    local SSH2_IP=$(echo $SSH2_CLIENT | awk '{ print $1 }')
    if [ $SSH2_IP ] || [ $SSH_IP ]; then
        PS1+="(\[${PURPLE}\]\u@\h"
    else
        PS1+="(\[${PURPLE}\]\u"
    fi

    # Current directory
    PS1+="\[${LIGHTGRAY}\]:\[${BROWN}\]\w\[${LIGHTGRAY}\])-"

    # Total size of files in current directory
    PS1+="(\[${GREEN}\]$(/bin/ls -lah | /bin/grep -m 1 total | /bin/sed 's/total //')\[${LIGHTGRAY}\]:"

    # Number of files
    PS1+="\[${GREEN}\]\$(/bin/ls -A -1 | /usr/bin/wc -l)\[${LIGHTGRAY}\])-"

    # git branch
    PS1+="(\[${LIGHTGRAY}\]\$(git branch --show-current 2>/dev/null)\[${LIGHTGRAY}\])"

    # Skip to the next line
    PS1+="\n"

    if [[ $EUID -ne 0 ]]; then
        PS1+="\[${GREEN}\] >\[${NOCOLOR}\] " # Normal user
    else
        PS1+="\[${RED}\]>\[${NOCOLOR}\] " # Root user
    fi

    # PS2 is used to continue a command using the \ character
    PS2="\[${LIGHTGRAY}\]>\[${NOCOLOR}\] "

    # PS3 is used to enter a number choice in a script
    PS3='Please enter a number from above list: '

    # PS4 is used for tracing a script in debug mode
    PS4='\[${LIGHTGRAY}\]+\[${NOCOLOR}\] '
}
PROMPT_COMMAND='__setprompt'


#######################################################
# SSH
#######################################################
eval $(keychain --eval --quiet ry_ecdsa)
eval $(keychain --eval --quiet id_rsa)
