#!/usr/bin/env bash

export MOZ_ENABLE_WAYLAND=1
export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache
export XDG_DATA_HOME=~/.local/share
export BROWSER=/usr/bin/brave-browser
export VISUAL=vim
export EDITOR=vim
export QT_QPA_PLATFORMTHEME=qt5ct
export DESKTOP_SESSION=sway
[ "$(tty)" = "/dev/tty1" ] && exec sway
