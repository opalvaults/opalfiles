#!/usr/bin/env python3

from PyQt5.QtCore import QUrl
from qutebrowser.api import interceptor
#import dracula.draw

# Load existing settings made via :set
config.load_autoconfig()
#dracula.draw.blood(c, {"spacing": {"vertical": 6, "horizontal": 8}})


# def intercept(info: interceptor.Request):
#     if info.request_url.host() == "youtube.com":
#         new_url = QUrl(info.request_url)
#         new_url.setHost("vid.puffyan.us")
#         try:
#             info.redirect(new_url)
#         except interceptor.interceptors.RedirectException:
#             pass


# interceptor.register(intercept)

c.qt.highdpi = True
c.fonts.default_size = "15pt"
c.fonts.tabs.selected = "13pt default_family"
c.fonts.tabs.unselected = "13pt default_family"
c.zoom.default = "130%"
c.content.javascript.enabled = False
c.downloads.location.directory = "~/downloads"
c.editor.command = ["alacritty", "-e", "vim", "{}"]
c.editor.encoding = "utf-8"
c.auto_save.session = True
c.colors.webpage.preferred_color_scheme = "dark"

# privacy
c.content.cookies.accept = "no-3rdparty"
c.content.webrtc_ip_handling_policy = "default-public-interface-only"
c.content.site_specific_quirks.enabled = False
c.content.headers.user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.79 Safari/537.36"

config.bind("j", "scroll-px 0 100")
config.bind("k", "scroll-px 0 -100")
config.bind("K", "tab-next")
config.bind("J", "tab-prev")

c.url.searchengines = {
    "DEFAULT": "https://duckduckgo.com/?q={}",
}

c.url.start_pages = ["http://localhost:80"]

# Bitwarden Password Management
config.bind(
    ",p",
    "spawn --userscript qute-bitwarden --dmenu-invocation 'bemenu -b -x --prompt=Pass'",
)

config.bind(
    ",u",
    "spawn --userscript qute-bitwarden -e --dmenu-invocation 'bemenu -b -p 'Username Only''",
)

config.bind(
    ",P",
    "spawn --userscript qute-bitwarden --dmenu-invocation 'bemenu -b -x -p 'Password Only''",
)
