# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments

## Exports ##
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/bin:$PATH

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

## Paths ##
# Scripts Path
PATH=$PATH:~/code/scripts

# Local Bin
PATH=$PATH:~/.local/bin

# Doom Path
PATH=$PATH:~/.config/emacs/bin/


# Load aliases and shortcuts if existent.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zsh_aliases" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zsh_aliases"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zsh_functions" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zsh_functions"
