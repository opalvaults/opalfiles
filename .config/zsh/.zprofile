# Each new shell auto-imports all environment variables.
# Hence exporting needs to be done only once.
# Also, all non-login shells are descendants of a login shell.
# Ergo, exports need to be done in the login shell only.
# Hence, we put exports in .zprofile

# Only vars needed by external commands should be exported.
# Note that you can export vars w/out assigning a value to them.

eval $(keychain --eval --quiet ry_ecdsa)
eval $(keychain --eval --quiet id_rsa)
export MOZ_ENABLE_WAYLAND=1
export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache
export XDG_DATA_HOME=~/.local/share
export PASSWORD_STORE_ENABLE_EXTENSIONS=true
export BROWSER=/usr/bin/firefox
export VISUAL=vim
export EDITOR=vim
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export HISTFILE="$XDG_CONFIG_HOME/zsh/zsh_history"
