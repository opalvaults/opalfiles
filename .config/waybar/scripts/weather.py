#!/usr/bin/env python3

import requests
import json
import sys

API_KEY = "99631af2d6db903d1f689c7d2cb13764"
CITY_ID = "5809844"
UNITS = "metric"


def celsius_to_fahrenheit(celsius):
    return (celsius * 9 / 5) + 32


def get_weather_icon(description):
    icons = {
        "clear sky": "󰖙",
        "few clouds": "",
        "scattered clouds": "",
        "broken clouds": "",
        "overcast clouds": "",
        "shower rain": "",
        "light rain": "",
        "rain": "",
        "moderate rain": "",
        "thunderstorm": "",
        "snow": "",
        "mist": "󰖑",
        "haze": "󰖑",
        "smoke": "󱞙"
    }
    return icons.get(description, "❓")


try:
    response = requests.get(
        f"http://api.openweathermap.org/data/2.5/weather?id={CITY_ID}&units={UNITS}&appid={API_KEY}"
    )
    response.raise_for_status()
    weather_data = response.json()

    weather_desc = weather_data["weather"][0]["description"]
    #print(weather_desc)
    temp_c = round(weather_data["main"]["temp"])
    temp_f = round(celsius_to_fahrenheit(temp_c))
    weather_icon = get_weather_icon(weather_desc)

    print(f"{weather_icon} {temp_c}°C / {temp_f}°F")

except requests.exceptions.RequestException as e:
    print(f"Error: {e}")
    print(json.dumps({"text": "", "tooltip": "Could not retrieve weather data"}))
    sys.exit(1)

except json.JSONDecodeError:
    print("Error: Failed to parse JSON response")
    print(response.content)
    sys.exit(1)
