#!/usr/bin/env python3

import subprocess


def get_playerctl_metadata(field):
    result = subprocess.run(
        ["playerctl", "--player=strawberry", "metadata", field],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )
    if result.returncode == 0:
        return result.stdout.strip()
    return None


def abbreviate(text, max_length=30):
    if len(text) > max_length:
        return text[: max_length - 3] + "..."
    return text


artist = get_playerctl_metadata("artist")
title = get_playerctl_metadata("title")

if artist and title:
    display_text = f"{artist} - {title}"
    display_text = abbreviate(display_text, 40)  # Adjust the max_length as needed
    print(f"󰝚 {display_text}")
